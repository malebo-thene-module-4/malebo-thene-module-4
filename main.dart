import 'package:animated_splash_screen/animated_splash_screen.dart' show AnimatedSplashScreen;
import 'package:flutter/material.dart';

void main()
{
  runApp(const MyApp());
}
class MyApp extends StatelessWidget {
  const MyApp({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const SplashScreen(),
      );
  }
}
class SplashScreen extends StatelessWidget {
  const SplashScreen({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimatedSplashScreen(
      splash: Column(
        children: [
          Image.asset('assets/86278-bat-flying-perfect-loop-animation.gif'),
          const Text('MTNAPP',style: TextStyle(fontSize: 40,fontWeight:FontWeight.bold)),
        ],),
         nextScreen: const MyApp(),
      
    );
  }
}
      
  